var
  // modules
  gulp = require('gulp'),
  concat = require('gulp-concat'),
  //uglify = require('gulp-uglify'),
  cleanCSS = require('gulp-clean-css')
  imagemin = require('gulp-imagemin');
;

// JavaScript processing

gulp.task('js', function() {

  // Minify the custom.js file.
  gulp.src('c:/xampp/htdocs/instagram/js/custom.js')
        .pipe(uglify())
        .pipe(gulp.dest('c:/xampp/htdocs/instagram/gulp-js/'));

  // Concatenate umbrella.js and custom.cs.  The files must be in the
  // order specified below.  The js won't work if umbrella.js is not first.
  // Also note that umbrella.js was already minified.
  gulp.src([
    'c:/xampp/htdocs/instagram/js/umbrella.js',
    'c:/xampp/htdocs/instagram/gulp-js/custom.js'])
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('c:/xampp/htdocs/instagram/js/'));

});

// CSS processing

gulp.task('css', function () {

    // Copy the fontawesome file to the work directory. It is already minified.
    gulp.src('c:/xampp/htdocs/instagram/css/all.min.css')
        .pipe(gulp.dest('c:/xampp/htdocs/instagram/gulp-css/'));

    // Minify the tailwinds.css file.
    gulp.src('c:/xampp/htdocs/instagram/css/tw.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('c:/xampp/htdocs/instagram/gulp-css/'));

    // Concatenate the files.
    gulp.src('c:/xampp/htdocs/instagram/gulp-css/**/*')
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('c:/xampp/htdocs/instagram/css/'));

});

// image processing

gulp.task('images', function () {
  gulp.src('C:/xampp/htdocs/instagram/img-unoptimized/**/*')
      .pipe(imagemin(
        [
          imagemin.gifsicle({interlaced: true}),
          imagemin.optipng({optimizationLevel: 5})
        ]
      ))
      .pipe(gulp.dest('C:/xampp/htdocs/instagram/img/'))


});

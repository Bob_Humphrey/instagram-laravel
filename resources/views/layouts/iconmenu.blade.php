<div>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlAbout }}">
    <i class="fas fa-info-circle"></i>
  </a>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlHashtags }}">
    <i class="fas fa-hashtag"></i>
  </a>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlAddHashtag }}">
    <i class="fas fa-plus-circle"></i>
  </a>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlCreateList }}">
    <i class="fas fa-list"></i>
  </a>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlLoginLogout }}">
    <i class="fas fa-unlock-alt"></i>
  </a>
</div>

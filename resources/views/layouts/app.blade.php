@php
  $env = env('APP_ENV');
  $counter = 0;
  $bg_color = ($counter % 2) ? "bg-white" : "bg-grey-light";
  $logoUrl = asset('img/bh-logo.png');
  $cssTw = asset('css/tw.css');
  $cssFontawesome = asset('css/all.min.css');
  $cssMin = asset('css/app.min.css');
  $urlAbout = route('about');
  $urlHashtags = action('HashtagController@index');
  $urlAddHashtag = action('HashtagController@create');
  $urlCreateList = action('BestHashtagController@showForm');
  $urlLogin = route('login');
  $urlLogout = action('LogoutController@logout');
  $urlLoginLogout = (Auth::check()) ? $urlLogout : $urlLogin;
@endphp

<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="This is an application containing tools for
    managing an Instagram account.">
    <title>
      Instagram Manager
    </title>

    @if ($env === 'local')
      <!-- Fontawesome -->
      <link rel="stylesheet" href="{{ $cssFontawesome }}">
      <!-- Styles -->
      <link rel="stylesheet" href="{{ $cssTw }}">
    @else
      <link rel="stylesheet" href="{{ $cssMin }}">
    @endif
  </head>

  <body class="bg-white">

    {{-- SMALL SCREENS --}}

    @php
      $iconColor = 'text-grey-darkest';
    @endphp

    <nav class="md:hidden bg-lavendar mb-12 py-6 text-white">
      <div class="flex justify-center ">
        <div class="mb-6">
          <a class="text-white no-underline" href="{{ $urlHashtags }}">
            <h1 class="font-l">Instagram Manager</h1>
          </a>
        </div>
      </div>
      <div class="flex justify-center text-xl text-grey-darkest">
        @include('layouts.iconmenu')
      </div>
    </nav>

    {{-- MEDIUM SCREENS --}}

    @php
      $iconColor = 'text-white';
    @endphp

    <nav class="hidden md:flex justify-between lg:hidden bg-lavendar mb-12 px-12 py-6 text-white">
      <div class="">
        <a class="text-white no-underline" href="{{ $urlHashtags }}">
          <h1 class="font-l leading-none">Instagram Manager</h1>
        </a>
      </div>
      <div class="flex items-center text-right text-xl">
        @include('layouts.iconmenu')
      </div>
    </nav>

    {{-- LARGE SCREENS --}}

    <nav class="hidden lg:flex justify-between bg-grey md:bg-dusty lg:bg-lavendar mb-12 px-12 py-6 text-white">
      <div>
        <a class="text-white no-underline" href="{{ $urlHashtags }}">
          <h1 class="font-l leading-none">Instagram Manager</h1>
        </a>
      </div>
      <div class="flex items-center text-right">
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlAbout }}">About</a></div>
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlHashtags }}">Hashtags</a></div>
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlAddHashtag }}">Add Hashtag</a></div>
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlCreateList }}">Create List</a></div>
        @if (Auth::check())
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlLogout }}">Logout</a></div>
        @else
        <div class="font-r px-6"><a class="text-white no-underline"
          href="{{ $urlLogin }}">Login</a></div>
        @endif
      </div>
    </nav>

    @include('layouts.flash')

    @include('layouts.errors')

    @yield('content')

    <footer class="py-16 bg-grey-light mt-12">
      <div class="flex justify-center w-full">
        <a href="https://bob-humphrey.com/">
          <img class="w-16 m-auto" src="{{ $logoUrl }}" alt="Bob Humphrey"/>
        </a>
      </div>
    </footer>

  </body>
</html>

<nav class="flex justify-between py-6">
  @if ($hashtags->currentPage() > 1)
    <div class="bg-lavendar hover:bg-grey-darker font-r font-bold py-2 px-8 rounded">
      <a class="text-white no-underline" href="{{ $hashtags->previousPageUrl() }}" >Prev Page</a>
    </div>
  @else
    <div class="bg-grey-light hover:bg-grey-light font-r font-bold py-2 px-8 rounded text-white">
      Prev Page
    </div>
  @endif

  @if ($hashtags->hasMorePages())
  <div class="bg-lavendar hover:bg-grey-darker font-r font-bold py-2 px-8 rounded">
    <a class="text-white no-underline" href="{{ $hashtags->nextPageUrl() }}" >Next Page</a>
  </div>
  @else
  <div class="bg-grey-light hover:bg-grey-light font-r font-bold py-2 px-8 rounded text-white">
    Next Page
  </div>
  @endif
</nav>

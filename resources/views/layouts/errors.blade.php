@if (count($errors))

<div class="flex justify-center w-full mb-4">
  <div class="w-9/10 lg:w-2/3 font-r leading-normal bg-red-lighter border border border-red-lighter rounded p-6">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
</div>

@endif

@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">

    <h2 class="font-l mb-4">Login</h2>

    <form class="w-full border-2 border-paper rounded py-12 px-6" method="POST" action="{{ route('login') }}">

      {{ csrf_field() }}

      <div class="md:flex items-center mb-3">
        <div class="md:w-1/6">
          <label class="block text-lavendar font-r font-bold md:text-right mb-1 mb-0 pr-4" for="email">
            Email
          </label>
        </div>
        <div class="md:w-5/6">
          <input class="bg-paper appearance-none border-2 border-paper rounded w-full py-2 px-4 text-grey-darker font-r leading-tight focus:outline-none focus:bg-white focus:border-lavendar"
            id="email" type="email" name="email" value="{{ old('email') }}" >
        </div>
      </div>

      <div class="md:flex items-center mb-3">
        <div class="md:w-1/6">
          <label class="block text-lavendar font-r font-bold md:text-right mb-1 mb-0 pr-4" for="password">
            Password
          </label>
        </div>
        <div class="md:w-5/6">
          <input class="bg-paper appearance-none border-2 border-paper rounded w-full py-2 px-4 text-grey-darker font-r leading-tight focus:outline-none focus:bg-white focus:border-lavendar"
            id="password" type="password" name="password" >
        </div>
      </div>

      <div class="md:flex items-center">
        <div class="md:w-1/6">
        </div>
        <div class="md:w-5/6 flex items-start">
            <input class="bg-lavendar hover:bg-grey-darker text-white font-r font-bold py-2 px-8 rounded" type="submit" name="Login" value="Login">
        </div>
      </div>

    </form>

  </div>
</div>
@endsection

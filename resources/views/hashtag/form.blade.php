<form class="w-full border-2 border-paper rounded py-12 px-6" method="{{ $formMethod }}" action="{{ $formAction }}">

  {{ csrf_field() }}

  @if ($parent === 'edit')
    <input name="_method" type="hidden" value="PATCH">
  @elseif ($parent === 'delete')
    <input name="_method" type="hidden" value="DELETE">
  @endif

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="block text-lavendar font-r font-bold md:text-right mb-1 mb-0 pr-4" for="name">
        Name
      </label>
    </div>
    <div class="md:w-5/6">
      <input class="bg-paper appearance-none border-2 border-paper rounded w-full py-2 px-4 text-grey-darker font-r leading-tight focus:outline-none focus:bg-white focus:border-lavendar"
        id="name" name="name" type="text" value="{{ $name }}" {{ $disabled }}>
    </div>
  </div>

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="block text-lavendar font-r font-bold md:text-right mb-1 mb-0 pr-4" for="rating">
        Rating
      </label>
    </div>
    @if ($disabled)
      <div class="md:w-5/6">
        <input class="bg-paper appearance-none border-2 border-paper rounded w-full py-2 px-4 text-grey-darker font-r leading-tight focus:outline-none focus:bg-white focus:border-lavendar"
          type="text" id="rating" name="rating" value="{{ $rating }}" {{ $disabled }}>
      </div>
    @else
      <div class="md:w-5/6">
        <select class="block appearance-none w-full bg-paper border border-paper text-grey-darker font-r py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-lavendar" id="rating" name="rating">
          <option {{ $selectOptions[0] }}>1</option>
          <option {{ $selectOptions[1] }}>2</option>
          <option {{ $selectOptions[2] }}>3</option>
          <option {{ $selectOptions[3] }}>4</option>
          <option {{ $selectOptions[4] }}>5</option>
          <option {{ $selectOptions[5] }}>6</option>
          <option {{ $selectOptions[6] }}>7</option>
          <option {{ $selectOptions[7] }}>8</option>
          <option {{ $selectOptions[8] }}>9</option>
          <option {{ $selectOptions[9] }}>10</option>
        </select>
      </div>
    @endif
  </div>

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="block text-lavendar font-r font-bold md:text-right mb-1 mb-0 pr-4" for="posts">
        Posts
      </label>
    </div>
    <div class="md:w-5/6">
      <input class="bg-paper appearance-none border-2 border-paper rounded w-full py-2 px-4 text-grey-darker font-r leading-tight focus:outline-none focus:bg-white focus:border-lavendar"
        id="posts" name="posts" type="text" value="{{ $posts }}" {{ $disabled }}>
    </div>
  </div>

  {{-- SMALL SCREENS --}}

  <div class="md:hidden">
    <div class="flex mb-2">
      <input type="checkbox" name="artistic" value="1" {{ $checkedArtistic }}>
      <div class="block text-lavendar font-r font-bold pl-4 pr-1">
        Artistic
      </div>
    </div>
    <div class="flex mb-2">
      <input type="checkbox" name="travel" value="1" {{ $checkedTravel }}>
      <div class="block text-lavendar font-r font-bold pl-4 pr-1">
        Travel
      </div>
    </div>
    <div class="flex mb-2">
      <input type="checkbox" name="environment" value="1" {{ $checkedEnvironment }}>
      <div class="block text-lavendar font-r font-bold pl-4 pr-1">
        Env
      </div>
    </div>
    <div class="flex mb-4">
      <input type="checkbox" name="urban" value="1" {{ $checkedUrban }}>
      <div class="block text-lavendar font-r font-bold pl-4 pr-1">
        Urban
      </div>
    </div>
  </div>

  {{-- MEDIUM AND LARGER SCREENS --}}

  <div class="hidden md:flex items-center mb-3">
    <div class="md:w-1/6">
    </div>
    <div class="md:w-5/6 flex items-start">
      <input type="checkbox" name="artistic" value="1" {{ $checkedArtistic }}>
      <div class="block text-lavendar font-r font-bold pl-1 pr-6">
        Artistic
      </div>
      <input type="checkbox" name="travel" value="1" {{ $checkedTravel }}>
      <div class="block text-lavendar font-r font-bold pl-1 pr-6">
        Travel
      </div>
      <input type="checkbox" name="environment" value="1" {{ $checkedEnvironment }}>
      <div class="block text-lavendar font-r font-bold pl-1 pr-6">
        Env
      </div>
      <input type="checkbox" name="urban" value="1" {{ $checkedUrban }}>
      <div class="block text-lavendar font-r font-bold pl-1 pr-6">
        Urban
      </div>
    </div>
  </div>

  <div class="flex items-center">
    <div class="md:w-1/6">
    </div>
    <div class="md:w-5/6 flex items-start">

      @if ($parent === 'create')
        <input class="bg-lavendar hover:bg-grey-darker text-white font-r font-bold py-2 px-8 rounded" type="submit" name="Add" value="Add">
      @elseif ($parent === 'show')
        <a class="text-lavendar font-r mr-6" href="{{ $urlActionEdit }}">Edit</a>
        <a class="text-lavendar font-r" href="{{ $urlActionDelete }}">Delete</a>
      @elseif ($parent === 'edit')
        <input class="bg-lavendar hover:bg-grey-darker text-white font-r font-bold py-2 px-8 rounded" type="submit" name="Update" value="Update">
      @elseif ($parent === 'delete')
        <input class="bg-lavendar hover:bg-grey-darker text-white font-r font-bold py-2 px-8 rounded" type="submit" name="Delete" value="Delete">
      @endif

    </div>
  </div>


</form>

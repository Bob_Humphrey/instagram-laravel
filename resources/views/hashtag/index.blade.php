@extends('layouts.app')

@section('content')

@php
  $counter = 0;
  $bg_color = ($counter % 2) ? "bg-white" : "bg-paper";
  $urlNameSort = url('hashtags') . "?sortField=name";
  $urlRatingSort = url('hashtags') . "?sortField=rating";
  $urlPostsSort = url('hashtags') . "?sortField=posts";
  $urlArtisticSort = url('hashtags') . "?sortField=artistic";
  $urlTravelSort = url('hashtags') . "?sortField=travel";
  $urlEnvironmentSort = url('hashtags') . "?sortField=environment";
  $urlUrbanSort = url('hashtags') . "?sortField=urban";

  // Indicate which grid column is being sorted.
  $sortAscending = '<i class="far fa-caret-square-up"></i>';
  $sortDescending = '<i class="far fa-caret-square-down"></i>';
  $nameSortInd = '';
  $ratingSortInd = '';
  $postsSortInd = '';
  $artisticSortInd = '';
  $travelSortInd = '';
  $envSortInd = '';
  $urbanSortInd = '';
  if (($sortField === 'name') && ($sortDirection === 'asc')) {
    $nameSortInd = $sortAscending;
  }
  elseif (($sortField === 'name') && ($sortDirection === 'desc')) {
    $nameSortInd = $sortDescending;
  }
  elseif (($sortField === 'rating') && ($sortDirection === 'asc')) {
    $ratingSortInd = $sortAscending;
  }
  elseif (($sortField === 'rating') && ($sortDirection === 'desc')) {
    $ratingSortInd = $sortDescending;
  }
  elseif (($sortField === 'posts') && ($sortDirection === 'asc')) {
    $postsSortInd = $sortAscending;
  }
  elseif (($sortField === 'posts') && ($sortDirection === 'desc')) {
    $postsSortInd = $sortDescending;
  }
  elseif (($sortField === 'artistic') && ($sortDirection === 'asc')) {
    $artisticSortInd = $sortAscending;
  }
  elseif (($sortField === 'artistic') && ($sortDirection === 'desc')) {
    $artisticSortInd = $sortDescending;
  }
  elseif (($sortField === 'travel') && ($sortDirection === 'asc')) {
    $travelSortInd = $sortAscending;
  }
  elseif (($sortField === 'travel') && ($sortDirection === 'desc')) {
    $travelSortInd = $sortDescending;
  }
  elseif (($sortField === 'environment') && ($sortDirection === 'asc')) {
    $envSortInd = $sortAscending;
  }
  elseif (($sortField === 'environment') && ($sortDirection === 'desc')) {
    $envSortInd = $sortDescending;
  }
  elseif (($sortField === 'urban') && ($sortDirection === 'asc')) {
    $urbanSortInd = $sortAscending;
  }
  elseif (($sortField === 'urban') && ($sortDirection === 'desc')) {
    $urbanSortInd = $sortDescending;
  }
@endphp

{{-- SMALL SCREENS --}}

<div class="flex justify-center w-full lg:hidden">
  <div class="w-9/10">
    <h2 class="font-l mb-4">Hashtags</h2>
    <table class="w-full">

      <tr>
        <th class="table-cell text-left {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlNameSort }}">Name {!! $nameSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlRatingSort }}">Rating {!! $ratingSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}">Actions</th>
      </tr>

      @foreach ($hashtags as $hashtag)
        @php
          $counter++;
          $bg_color = ($counter % 2) ? "bg-white" : "bg-paper";
          $nameUrl = "https://www.instagram.com/explore/tags/" . $hashtag->name . "/";
          $urlActionShow = action('HashtagController@show', ['id' => $hashtag->id]);
          $urlActionEdit = action('HashtagController@edit', ['id' => $hashtag->id]);
          $urlActionDelete = action('HashtagController@delete', ['id' => $hashtag->id]);
        @endphp

        <tr>
          <td class="table-cell {{ $bg_color }}"><a class="text-black no-underline" href="{{ $nameUrl }}" target="_blank">{{ $hashtag->name }}</a></td>
          <td class="table-cell text-center {{ $bg_color }}">{{ $hashtag->rating }}</td>
          <td class="table-cell text-center {{ $bg_color }}">
            <a class="text-black no-underline" href="{{ $urlActionShow }}"><i class="far fa-eye"></i></a>
            <a class="text-black no-underline px-2" href="{{ $urlActionEdit }}"><i class="far fa-edit"></i></a>
            <a class="text-black no-underline" href="{{ $urlActionDelete }}"><i class="far fa-trash-alt"></i></a>
          </td>
        </tr>

    @endforeach

    </table>

    @include('layouts.pagination')

  </div>
</div>

{{-- LARGE SCREENS --}}

<div class="hidden lg:flex justify-center w-full">
  <div class="w-2/3">
    <h2 class="font-l mb-4">Hashtags</h2>
    <table class="w-full">

      <tr>
        <th class="table-cell text-left {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlNameSort }}">Name {!! $nameSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlRatingSort }}">Rating {!! $ratingSortInd !!}</a></th>
        <th class="table-cell text-right {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlPostsSort }}">Posts {!! $postsSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlArtisticSort }}">Artistic {!! $artisticSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlTravelSort }}">Travel {!! $travelSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlEnvironmentSort }}">Env {!! $envSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}"><a class="text-black no-underline" href="{{ $urlUrbanSort }}">Urban {!! $urbanSortInd !!}</a></th>
        <th class="table-cell text-center {{ $bg_color }}">Actions</th>
      </tr>

      @foreach ($hashtags as $hashtag)
        @php
          $counter++;
          $bg_color = ($counter % 2) ? "bg-white" : "bg-paper";
          $nameUrl = "https://www.instagram.com/explore/tags/" . $hashtag->name . "/";
          $posts = number_format($hashtag->posts);
          $artistic = $hashtag->artistic ? "<i class='far fa-star'></i>" : " ";
          $travel = $hashtag->travel ? "<i class='far fa-star'></i>" : " ";
          $environment = $hashtag->environment ? "<i class='far fa-star'></i>" : " ";
          $urban = $hashtag->urban ? "<i class='far fa-star'></i>" : " ";
          $urlActionShow = action('HashtagController@show', ['id' => $hashtag->id]);
          $urlActionEdit = action('HashtagController@edit', ['id' => $hashtag->id]);
          $urlActionDelete = action('HashtagController@delete', ['id' => $hashtag->id]);
        @endphp

        <tr>
          <td class="table-cell {{ $bg_color }}"><a class="text-black no-underline" href="{{ $nameUrl }}" target="_blank">{{ $hashtag->name }}</a></td>
          <td class="table-cell text-center {{ $bg_color }}">{{ $hashtag->rating }}</td>
          <td class="table-cell text-right {{ $bg_color }}">{{ $posts }}</td>
          <td class="table-cell text-center {{ $bg_color }}">{!! $artistic !!}</td>
          <td class="table-cell text-center {{ $bg_color }}">{!! $travel !!}</td>
          <td class="table-cell text-center {{ $bg_color }}">{!! $environment !!}</td>
          <td class="table-cell text-center {{ $bg_color }}">{!! $urban !!}</td>
          <td class="table-cell text-center {{ $bg_color }}">
            <a class="text-black no-underline" href="{{ $urlActionShow }}"><i class="far fa-eye"></i></a>
            <a class="text-black no-underline px-2" href="{{ $urlActionEdit }}"><i class="far fa-edit"></i></a>
            <a class="text-black no-underline" href="{{ $urlActionDelete }}"><i class="far fa-trash-alt"></i></a>
          </td>
        </tr>

    @endforeach

    </table>

    @include('layouts.pagination')

  </div>
</div>

@endsection

@extends('layouts.app')

@section('content')

@php
  $urlActionDestroy = action('HashtagController@destroy', ['id' => $hashtag->id]);
  $name = $hashtag->name;
  $posts = $hashtag->posts;
  $rating = $hashtag->rating;
  // Determine checkbox values
  $checkedArtistic = ($hashtag->artistic == 1) ? 'checked' : '';
  $checkedTravel = ($hashtag->travel == 1) ? 'checked' : '';
  $checkedEnvironment = ($hashtag->environment == 1) ? 'checked' : '';
  $checkedUrban = ($hashtag->urban == 1) ? 'checked' : '';
  $disabled = 'disabled';
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-2">Delete Hashtag</h2>
    <div class="font-l text-red font-bold mb-4">
      Warning! This action cannot be undone.
    </div>

    @include('hashtag.form', [
      'parent' => 'delete',
      'formMethod' => 'POST',
      'formAction' => $urlActionDestroy
      ])

  </div>
</div>

@endsection

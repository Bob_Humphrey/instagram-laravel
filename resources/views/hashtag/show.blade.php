@extends('layouts.app')

@section('content')

@php
  $urlActionEdit = action('HashtagController@edit', ['id' => $hashtag->id]);
  $urlActionDelete = action('HashtagController@delete', ['id' => $hashtag->id]);
  $name = $hashtag->name;
  $posts = $hashtag->posts;
  $rating = $hashtag->rating;
  // Determine checkbox values
  $checkedArtistic = ($hashtag->artistic == 1) ? 'checked' : '';
  $checkedTravel = ($hashtag->travel == 1) ? 'checked' : '';
  $checkedEnvironment = ($hashtag->environment == 1) ? 'checked' : '';
  $checkedUrban = ($hashtag->urban == 1) ? 'checked' : '';
  $disabled = 'disabled';
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">View Hashtag</h2>

    @include('hashtag.form', [
      'parent' => 'show',
      'formMethod' => 'GET',
      'formAction' => $urlActionEdit
      ])

  </div>
</div>

@endsection

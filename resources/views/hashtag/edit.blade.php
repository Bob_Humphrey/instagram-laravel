@extends('layouts.app')

@section('content')

@php
  $urlActionUpdate = action('HashtagController@update', ['id' => $hashtag->id]);
  $urlActionIndex = action('HashtagController@index');
  $name = old('name') ? old('name') : $hashtag->name;
  $posts = old('posts') ? old('posts') : $hashtag->posts;
  // Determine which option was selected
  $rating = old('rating') ? old('rating') : $hashtag->rating;
  $selectOptions = array();
  for ($x = 1; $x <= 10; $x++) {
    $selectOptions[$x - 1] = ($rating == $x) ? 'selected' : '';
  }
  // Determine checkbox values
  if (count($errors)) {
    $checkedArtistic = (old('artistic') == 1) ? 'checked' : '';
    $checkedTravel = (old('travel') == 1) ? 'checked' : '';
    $checkedEnvironment = (old('environment') == 1) ? 'checked' : '';
    $checkedUrban = (old('urban') == 1) ? 'checked' : '';
  }
  else {
    $checkedArtistic = ($hashtag->artistic == 1) ? 'checked' : '';
    $checkedTravel = ($hashtag->travel == 1) ? 'checked' : '';
    $checkedEnvironment = ($hashtag->environment == 1) ? 'checked' : '';
    $checkedUrban = ($hashtag->urban == 1) ? 'checked' : '';
  }
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">Edit Hashtag</h2>

    @include('hashtag.form', [
      'parent' => 'edit',
      'formMethod' => 'POST',
      'formAction' => $urlActionUpdate
      ])

  </div>
</div>

@endsection

@extends('layouts.app')

@section('content')

@php
  $urlStore = action('HashtagController@store');
  $name = old('name');
  $posts = old('posts');
  // Save input value if an option was already selected
  $selectOptions = array();
  for ($x = 1; $x <= 10; $x++) {
    $selectOptions[$x - 1] = (old('rating') == $x) ? 'selected' : '';
  }
  // Save input value if a checkbox was already checked
  $checkedArtistic = (old('artistic') == 1) ? 'checked' : '';
  $checkedTravel = (old('travel') == 1) ? 'checked' : '';
  $checkedEnvironment = (old('environment') == 1) ? 'checked' : '';
  $checkedUrban = (old('urban') == 1) ? 'checked' : '';
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">Add Hashtag</h2>

    @include('hashtag.form', [
      'parent' => 'create',
      'formMethod' => 'POST',
      'formAction' => $urlStore
      ])

  </div>
</div>

@endsection

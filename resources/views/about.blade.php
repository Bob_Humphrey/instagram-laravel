@extends('layouts.app')

@section('content')


<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">About</h2>
    <div class="font-r text-lavendar leading-normal lg:px-32">
      <p>
        Hashtags are added to Instagram posts to help get your images
        out to a wider audience.

        But there are thousands of different hashtags, and the question
        I've always had has been which particular tags should I be
        using with my photography?

        I created this application to help me try to work this out.
      </p>
      <p>
        When I come across a hashtag that I think would be effective
        in drawing viewers to my pictures, I add it to this app.
        I estimate how useful it might be and give it a rating
        between 1 and 10. I also assign it to one or more categories
        that apply to my different photography subject matter.
      </p>
      <p>
        Later, when I go to post a new image to Instagram, I make use of
        the Create List function to select 30 tags from my collection
        according to various criteria. The tags are displayed in
        a nice list that I can copy and paste into my new Instagram post.
      </p>


      <h3 class="font-l text-black mb-4 mt-4">Credits</h3>
      <div class="font-r text-lavendar leading-normal">
        This application was built with:
        <ul>
          <li>PHP 7.2.7</li>
          <li>Laravel 5.6</li>
          <li>MySQL</li>
          <li>Tailwind CSS 0.6.5</li>
          <li>Font Awesome Icons</li>
        </ul>
      </div>
    </div>
  </div>
</div>

@endsection

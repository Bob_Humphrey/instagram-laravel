@extends('layouts.app')

@section('content')

@php
  $formAction = action('BestHashtagController@showList');
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">Create Hashtag List</h2>

    <form class="w-full border-2 border-paper rounded py-12 px-6 md:px-24" method="GET" action="{{ $formAction }}">

      {{ csrf_field() }}

      <div class="">
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="artistic" checked>
          </div>
          <div class="block text-lavendar font-r font-bold">
            Artistic
          </div>
        </div>
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="travel">
          </div>
          <div class="block text-lavendar font-r font-bold">
            Travel
          </div>
        </div>
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="environment">
          </div>
          <div class="block text-lavendar font-r font-bold ">
            Environment
          </div>
        </div>
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="urban">
          </div>
          <div class="block text-lavendar font-r font-bold">
            Urban
          </div>
        </div>
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="most_posts">
          </div>
          <div class="block text-lavendar font-r font-bold">
            Most Posts
          </div>
        </div>
        <div class="flex mb-2">
          <div class="pr-1">
            <input type="radio" name="choice" value="least_posts">
          </div>
          <div class="block text-lavendar font-r font-bold">
            Least Posts
          </div>
        </div>
        <div class="flex mb-4">
          <div class="pr-1">
            <input type="radio" name="choice" value="rating">
          </div>
          <div class="block text-lavendar font-r font-bold">
            Highest Rating
          </div>
        </div>
      </div>

      <input class="bg-lavendar hover:bg-grey-darker text-white font-r font-bold py-2 px-8 rounded" type="submit" name="list" value="Create List">

    </form>
  </div>
</div>

@endsection

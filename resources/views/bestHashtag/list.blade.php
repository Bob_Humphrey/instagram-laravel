@extends('layouts.app')

@section('content')

@php
  $count = 0;
  $totalRatings = 0;
  $avgRating = 0.0;
  foreach ($hashtags as $hashtag) {
    $count++;
    $totalRatings += $hashtag->rating;
  }
  $avgRating = $totalRatings / $count;
  $avgRatingDisplay =  number_format($avgRating, 1);
@endphp

<div class="flex justify-center w-full">
  <div class="w-9/10 lg:w-2/3 ">
    <h2 class="font-l mb-4">Hashtag List</h2>
    <div class="font-r border-2 border-paper
      rounded py-12 px-12 text-lg text-lavendar mb-6" >
      @php
        foreach ($hashtags as $hashtag) {
          echo '#' . $hashtag->name . ' ';
        }
      @endphp
    </div>
    <div class="font-r text-grey-darkest">
        Average rating: {{ $avgRatingDisplay }}
    </div>
  </div>
</div>

@endsection

@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">

    <div class="form text-black m-auto text-4xl">
      Sorry, the page you are looking for could not be found.
    </div>

  </div>
</div>

@endsection

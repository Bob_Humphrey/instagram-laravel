<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', 'HashtagController@index');
Route::get('/home', 'HomeController@index')->name('home');

// View routes
Route::view('about', 'about')->name('about');;

// HashtagController
Route::get('/hashtags/{id}/delete', 'HashtagController@delete');
Route::resource('hashtags', 'HashtagController');

// BestHashtagController
Route::get('/besthashtags/form', 'BestHashtagController@showForm');
Route::get('/besthashtags/list', 'BestHashtagController@showList');

//Auth::routes();
Route::get('/customlogout', 'LogoutController@logout');

Route::get('/login', 'Auth\LoginController@showLoginForm' )->name('login');
Route::post('/login', 'Auth\LoginController@login');
//Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Hashtag;

class HashtagController extends Controller
{

  public function __construct() {
    $this->middleware('auth', ['only' => ['store', 'update', 'destroy']]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //$request->session()->flush();
      $sortField = $request->input('sortField');
      $page = $request->input('page');

      // Get previous sort direction, if there is one.
      if ($sortField) {
        $sessionKey = $sortField . '_sort_direction';
        $previousSortDirection = session($sessionKey);
      }

      // There is no sortField or page.
      if (!$page && !$sortField) {
        $sortField = 'name';
        $sortDirection = 'asc';
      }

      // The user has selected a sortField, but no page, and there is no previousSortDirection.
      elseif (!$page && !$previousSortDirection) {
        $sortDirection = (($sortField === 'name') || ($sortField === 'posts')) ? 'asc' : 'desc';
      }

      // The user has selected a sortField and there is a previousSortDirection.
      elseif (!$page && $previousSortDirection) {
        $sortDirection = ($previousSortDirection == 'asc') ? 'desc' : 'asc';
      }

      // If the user has not selected a page, save the sort direction.
      if (!$page) {
        $sessionKey = $sortField . '_sort_direction';
        session([$sessionKey => $sortDirection]);
        session(['current_sort_field' => $sortField]);
        session(['current_sort_direction' => $sortDirection]);
      }

      // If the user has selected a page, get the current sortField and sortDirection.
      if ($page) {
        $sortField = session('current_sort_field');
        $sortDirection = session('current_sort_direction');
      }

      $hashtags = DB::table('hashtags')->orderBy($sortField, $sortDirection)->simplePaginate(30
    );
      return view('hashtag.index', [
        'hashtags' => $hashtags,
        'sortField' => $sortField,
        'sortDirection' => $sortDirection
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('hashtag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
          'name' => 'required|unique:hashtags,name',
          'rating'=> 'required|in:1,2,3,4,5,6,7,8,9,10',
          'posts' => 'required|numeric|integer'
        ]);
        Hashtag::create(request(['name', 'rating', 'posts', 'artistic',
          'travel', 'environment', 'urban']));
        $request->session()->flash('status', '#' . request()->name . ' has been added.');
        return redirect(action('HashtagController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hashtag = Hashtag::find($id);
        return view('hashtag.show', ['hashtag' => $hashtag]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $hashtag = Hashtag::find($id);
      return view('hashtag.edit', ['hashtag' => $hashtag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(), [
        'name' => 'required',
        'rating'=> 'required|in:1,2,3,4,5,6,7,8,9,10',
        'posts' => 'required|numeric|integer'
      ]);

      $hashtag = Hashtag::find($id);
      $hashtag->name = request()->name;
      $hashtag->posts = request()->posts;
      $hashtag->rating = request()->rating;
      $hashtag->artistic = request()->artistic ? 1 : 0;
      $hashtag->travel = request()->travel ? 1 : 0;
      $hashtag->environment = request()->environment ? 1 : 0;
      $hashtag->urban = request()->urban ? 1 : 0;
      $hashtag->save();
      $request->session()->flash('status', '#' . request()->name . ' has been updated.');
      return redirect(action('HashtagController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $hashtag = Hashtag::find($id);
      $name = $hashtag->name;
      $hashtag->delete();
      session()->flash('status', '#' . $name . ' has been deleted.');
      return redirect(action('HashtagController@index'));
    }

    /**
     * Display the specified resource to be deleted.
     * An extra step to protect against accidental deletes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $hashtag = Hashtag::find($id);
        return view('hashtag.delete', ['hashtag' => $hashtag]);
    }


}
